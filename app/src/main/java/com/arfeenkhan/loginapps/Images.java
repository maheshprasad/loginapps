package com.arfeenkhan.loginapps;

public class Images {
    String names;

    public Images() {
    }

    public Images(String names) {
        this.names = names;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }
}
