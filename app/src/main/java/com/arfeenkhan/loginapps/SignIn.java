package com.arfeenkhan.loginapps;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignIn extends AppCompatActivity {
    EditText edtemail, edtpass;
    String semail, spass;
    Button btn_signin;
    AlertDialog.Builder builder;
    String login_url = "http://192.168.220.2/userinfo/login.php";

    //user class to check user email is already availabel or not
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        //init widget
        edtemail = findViewById(R.id.edt_email);
        edtpass = findViewById(R.id.edt_password);
        btn_signin = findViewById(R.id.btn_sign_in);
        builder = new AlertDialog.Builder(this);
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signin();
            }
        });

        user = new User(SignIn.this);

        if (user.getName() != "") {
            startActivity(new Intent(SignIn.this, Welcome.class));
        } else {
            startActivity(new Intent(SignIn.this, SignIn.class));
        }

    }

    private void signin() {
        semail = edtemail.getText().toString();
        spass = edtpass.getText().toString();

        if (semail.equals("") || spass.equals("")) {
            builder.setTitle("Something went wrong");
            displayAlert("Enter a valid email and password....");
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, login_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String code = jsonObject.getString("code");
                        if (code.equals("login_failed")) {
                            builder.setTitle("Login Error...");
                            displayAlert(jsonObject.getString("message"));
                        } else {
                            user.setName(semail);
                            Intent intent = new Intent(SignIn.this, Welcome.class);
                            Bundle bundle = new Bundle();
//                            bundle.putString("name", jsonObject.getString("name"));
//                            bundle.putString("email", jsonObject.getString("email"));
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SignIn.this, "Something went wrong...", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", semail);
                    params.put("password", spass);
                    return params;
                }
            };
            MySingleton.getmInstance(SignIn.this).addToRequestque(stringRequest);
        }
    }

    public void displayAlert(String message) {
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                edtemail.setText("");
                edtpass.setText("");
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void NewUser(View view) {
        startActivity(new Intent(SignIn.this, SignUp.class));
    }
}
