package com.arfeenkhan.loginapps;

import android.content.Context;
import android.content.SharedPreferences;

public class User {
    SharedPreferences sharedPreferences;
    Context context;
    private String name;

    public void removeUser(){
        sharedPreferences.edit().clear().commit();
    }

    public String getName() {
        name=sharedPreferences.getString("userdata","");
        return name;
    }

    public void setName(String name) {
        this.name = name;
        sharedPreferences.edit().putString("userdata",name).commit();
    }

    public User(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("usersinfo", Context.MODE_PRIVATE);
    }
}
