package com.arfeenkhan.loginapps;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {
    private static final String TAG = "SignUp";
    EditText edtname, edtemail, edtusename, edtpass, edtconpas;
    String sname, semail, susername, spass, sconpass, codes, messages;
    Button btn_regist;
    AlertDialog.Builder builder;
    String url = "http://192.168.220.2/userinfo/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //init widget
        edtname = findViewById(R.id.edt_name);
        edtemail = findViewById(R.id.edt_email);
        edtusename = findViewById(R.id.edt_user_name);
        edtpass = findViewById(R.id.edt_password);
        edtconpas = findViewById(R.id.edt_confirm_password);
        btn_regist = findViewById(R.id.btn_register);
        builder = new AlertDialog.Builder(this);
        btn_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    private void register() {
        sname = edtname.getText().toString();
        semail = edtemail.getText().toString();
        susername = edtusename.getText().toString();
        spass = edtpass.getText().toString();
        sconpass = edtconpas.getText().toString();

        if (sname.equals("") || semail.equals("") || susername.equals("") || spass.equals("") || sconpass.equals("")) {
            builder.setTitle("Something went wrong....");
            builder.setMessage("Please fill all the fields...");
            displayAlert("input_error");
        } else {
            if (!spass.equals(sconpass)) {
                builder.setTitle("Something went wrong....");
                builder.setMessage("Your passwords are not matching...");
                displayAlert("input_error");
            } else {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // TODO: 8/22/2018
                        //get the response from server through volley
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject c = jsonArray.getJSONObject(0);
                            codes = c.getString("code");
                            messages = c.getString("message");
                            Log.i(TAG, "code " + codes);


                            builder.setTitle("Server Response...");
                            builder.setMessage(messages);
                            displayAlert(codes);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                    // TODO: 8/22/2018
                    //for pass data with String request in remote database
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("name", sname);
                        params.put("email", semail);
                        params.put("user_name", susername);
                        params.put("password", sconpass);
                        return params;
                    }
                };
                MySingleton.getmInstance(this).addToRequestque(stringRequest);
            }
        }
    }

    public void displayAlert(final String code) {
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (code.equals("input_error")) {
                    edtpass.setText("");
                    edtconpas.setText("");
                } else if (code.equals("reg_success")) {
                    finish();
                } else if (code.equals("reg_failed")) {
                    edtname.setText("");
                    edtusename.setText("");
                    edtemail.setText("");
                    edtpass.setText("");
                    edtconpas.setText("");
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void OldUser(View view) {
        startActivity(new Intent(SignUp.this, SignIn.class));
    }
}
