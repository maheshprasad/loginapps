package com.arfeenkhan.loginapps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    ArrayList<Images> list;
    Context context;

    public RecyclerAdapter(ArrayList<Images> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Images images = list.get(position);
        holder.mname.setText(images.getNames());
        Glide.with(context).load(images.getNames()).into(holder.mimg);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mimg;
        TextView mname;

        public MyViewHolder(View itemView) {
            super(itemView);
            mimg = itemView.findViewById(R.id.imageview);
            mname = itemView.findViewById(R.id.name);
        }
    }
}
