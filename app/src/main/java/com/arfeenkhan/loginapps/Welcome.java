package com.arfeenkhan.loginapps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Welcome extends AppCompatActivity implements View.OnClickListener {
    Button mupload, mchoose;
    EditText medtname;
    ImageView mimag;
    public static final int IMAGE_REQUEST = 1001;
    Bitmap bitmap;

    String url = "http://192.168.220.2/userinfo/imageupload.php";
    String imagename;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        //init widget
        mupload = findViewById(R.id.uploadimage);
        mchoose = findViewById(R.id.chooseimage);
        medtname = findViewById(R.id.medtname);
        mimag = findViewById(R.id.imageView);

        mupload.setOnClickListener(this);
        mchoose.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chooseimage:
                selectImage();
                break;
            case R.id.uploadimage:
                imagename = medtname.getText().toString();
                if (TextUtils.isEmpty(imagename)) {
                    Toast.makeText(this, "Enter Image Name", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.setTitle("Image Uploading");
                    progressDialog.setMessage("Please wait...");
                    progressDialog.show();
                    uploadImage();
                }
                break;
        }
    }

    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                mimag.setImageBitmap(bitmap);
                mimag.setVisibility(View.VISIBLE);
                medtname.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void uploadImage() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    Toast.makeText(Welcome.this, Response, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    mimag.setImageResource(0);
                    mimag.setVisibility(View.GONE);
                    medtname.setText("");
                    medtname.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", imagename);
                params.put("image", imageToString(bitmap));
                return params;
            }
        };

        MySingleton.getmInstance(Welcome.this).addToRequestque(stringRequest);

    }


    // TODO: 8/23/2018 convert image to string
    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

        //convert byteArrayOutputStream in byte array
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.recyclerid) {
            startActivity(new Intent(Welcome.this, RecyclerActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
